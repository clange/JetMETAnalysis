///////////////////////////////////////////////////////////////////
//
// jet_l2l3_correction_Minuit_x
// -------------------------
//
//            11/25/2011 Ricardo, trying to generate corrections by fitting
///////////////////////////////////////////////////////////////////

#include "JetMETAnalysis/JetAnalyzers/interface/Settings.h"
#include "JetMETAnalysis/JetUtilities/interface/CommandLine.h"

#include "CondFormats/JetMETObjects/interface/JetCorrectorParameters.h"
#include "CondFormats/JetMETObjects/interface/FactorizedJetCorrector.h"

#include "TROOT.h"
#include "TSystem.h"
#include "TFile.h"
#include "TTree.h"
#include "TF1.h"
#include "TH1.h"
#include "TH1F.h"
#include "TH1D.h"
#include "TH2.h"
#include "TH2F.h"
#include "TString.h"
#include "TMath.h"

#include <iostream>
#include <sstream>
#include <fstream>
#include <string>
#include <vector>
#include <stdio.h>
#include <stdarg.h>
#include <cstring>
#include <iomanip>

using namespace std;

////////////////////////////////////////////////////////////////////////////////
// define local functions
////////////////////////////////////////////////////////////////////////////////

/// get the bin number for a specific ptgen according to the vector of bin edges 
int getBin(double x, const double boundaries[NPtBins]);

/// get the uppercase version of the algorithm name
string getAlias(string s);

// do here all the analysis for this algo
void analyzeAlgo(string algo, TDirectoryFile * idir, CommandLine & cl);

// print here the corrections to file
void  printCorrectionsToFile(vector<TF1 *> ffit, string path, string era, string algo);

// do here the fit for the given algo in the given eta bin
TF1 * analyzeAlgoForEtaBin(string algo, TDirectoryFile * idir, CommandLine & cl, 
			   double mineta, double maxeta);

// create and return the fit function for the given algo 
TF1 * getFitFunctionAlgo(string alg, double xmin, double xmax);

// do here all the analysis for this algo
void whatever(string algo, TDirectoryFile * idir, CommandLine & cl);

////////////////////////////////////////////////////////////////////////////////
// main
////////////////////////////////////////////////////////////////////////////////

//______________________________________________________________________________
int main(int argc,char**argv)
{
  gSystem->Load("libFWCoreFWLite.so");
  
  //
  // evaluate command-line / configuration file options
  // 
  CommandLine cl;
  if (!cl.parse(argc,argv)) return 0;

  string         inputFilename     = cl.getValue<string>  ("inputFilename");
  vector<string> algs              = cl.getVector<string> ("algs");
  string         outputDir         = cl.getValue<string>  ("outputDir", "");
  //if (!cl.check()) return 0;
  cl.print();

  // Loop over the algorithms 
  for(unsigned int a=0; a<algs.size(); a++){

    // get the input file 
    TFile *inf = new TFile(inputFilename.c_str());

      // create the output file
    if(outputDir.length() > 0 && outputDir[outputDir.length()-1] != '/') outputDir += "/";
      string outf_str = outputDir+"L2L3Minuit_"+algs[a]+".root" ;
      TFile *outf = new TFile(outf_str.c_str(),"RECREATE");
      if (!outf) {
	cout<<" Output file="<<outf_str<<" could not be created"<<endl;
	exit(0);
      }
      
      // get the corresponding algo
      TDirectoryFile *idir = (TDirectoryFile*)inf->Get(algs[a].c_str());
      cout << "The directory is " << idir->GetName() << endl;

      // analyze that algorithm.
      analyzeAlgo(algs[a], idir, cl);

      // write and close the output file
      outf->cd();
      outf->Write();
      outf->Close();

    }// for algos

}//main

//---------------------------------------------------------------------
// Do here all the analysis for the given algorithm
void analyzeAlgo(string algo, TDirectoryFile * idir, CommandLine & cl){

  // Define the eta bins 
  //const int netabins = 83;
  double binseta[83] = 
    {-5.191, -4.889, -4.716, -4.538, -4.363, -4.191, -4.013, -3.839, -3.664, 
     -3.489, -3.314, -3.139, -2.964, -2.853, -2.650, -2.500, -2.322, -2.172, 
     -2.043, -1.930, -1.830, -1.740, -1.653, -1.566, -1.479, -1.392, -1.305, 
     -1.218, -1.131, -1.044, -0.957, -0.879, -0.783, -0.696, -0.609, -0.522, 
     -0.435, -0.348, -0.261, -0.174, -0.087,    
     +0.000,    
     +0.087, +0.174, +0.261, +0.348, +0.435, +0.522, +0.609, +0.696, +0.783, 
     +0.879, +0.957, +1.044, +1.131, +1.218, +1.305, +1.392, +1.479, +1.566, 
     +1.653, +1.740, +1.830, +1.930, +2.043, +2.172, +2.322, +2.500, +2.650, 
     +2.853, +2.964, +3.139, +3.314, +3.489, +3.664, +3.839, +4.013, +4.191, 
     +4.363, +4.538, +4.716, +4.889, +5.191};

  // holds the vector of the best fit for each eta.
  vector<TF1*> ffit;

  // Loop over all etas 
  //for (int ieta = 0; ieta < netabins-1; ieta++){
  for (int ieta = 41; ieta < 42; ieta++){

    // get the best fit for the given eta
    TF1 * eta_fit = analyzeAlgoForEtaBin(algo, idir, cl, binseta[ieta],binseta[ieta+1]);
    
    // set the range of eta just to store the eta bin region
    if (eta_fit) eta_fit->SetRange(binseta[ieta],binseta[ieta+1]);

    // put it in the vector from which the text file will be made
    ffit.push_back(eta_fit);

  }//for eta bins
			
  // Write the corrections to file
  string          era               = cl.getValue<string>   ("era");
  string          path              = cl.getValue<string>   ("path");
  printCorrectionsToFile(ffit, path, era, algo);

}// analyzeAlgo

//---------------------------------------------------------------------
// This returns the fit for the given algo in the given eta region
TF1 * analyzeAlgoForEtaBin(string algo, TDirectoryFile * idir, 
			   CommandLine & cl, double mineta, double maxeta){

  // get the tree to fit to
  TTree *tree = (TTree*)idir->Get("t");

  // get the fit function for this algorithm and set the range from 10 to 3500 GeV. 
  TF1 * res = getFitFunctionAlgo(algo, 10, 3500);

  // create the fitting formula
  //string fitform = "refpt-jtpt*fit(0)";

  // create a selection string
  string sel = "jtpt>10 && refdrjt <0.25 && 0<jteta && jteta<0.087";

  int es = tree->UnbinnedFit("fit","refpt/jtpt",sel.c_str(),"",10000);
  cout<<" error status="<<es<<endl;
  cout<<" entries used="<<tree->GetSelectedRows()<<endl;
  return res;

}//analyzeAlgoForEtaBin

//---------------------------------------------------------------------
// do here all the analysis for this algo
void whatever(string algo, TDirectoryFile * idir, CommandLine & cl){

  float refpt[100];
  float refeta[100];
  float refphi[100];
  float jtpt[100];
  float jteta[100];
  float jtphi[100];
  float refdrjt[100];
  //float refdphijt[100];
  int   refpdgid[100];
  TH2F *RespVsPt_Bar;
  TH2F *RespVsPt_End;
  TH2F *RespVsPt_Fwd;
  unsigned char nref;
  unsigned int nrefmax = 2;
 

  string alias = getAlias(algo);

  bool            doflavor          = cl.getValue<bool>     ("doflavor",          false);
  int             pdgid             = cl.getValue<int>      ("pdgid",                 0);
  unsigned int    nEvents           = cl.getValue<unsigned int>("nEvents",            0);
  double          drmax             = cl.getValue<double>   ("drmax",               0.25);
  string         weightfilename    = cl.getValue<string>  ("weightfilename",       "");

  //
  // To get weights for ptgen distribution
  //
  TFile *weightFile = 0;
  TH1D *weightHist = 0;
  if(weightfilename.length() != 0 ){
    weightFile = new TFile(weightfilename.c_str(),"READ");
      if (!weightFile->IsOpen()) {cout<<"Can't open ff.root to get weights for ptgen"<<endl;}
      weightHist = (TH1D*)gDirectory->Get("we");
      if (weightHist==0) {cout<<"weightHist named \"we\" was not in file ff.root"<<endl; return;}
      weightHist->Scale(1./weightHist->Integral(1,weightHist->FindBin(3)));
  }
  

  //
  // setup the tree for reading
  //
  TTree *tree = (TTree*)idir->Get("t");
  tree->SetBranchAddress("nref",   &nref);
  tree->SetBranchAddress("refpt",   refpt);
  tree->SetBranchAddress("refeta",  refeta);
  tree->SetBranchAddress("refphi",  refphi);
  tree->SetBranchAddress("jtpt",    jtpt);
  tree->SetBranchAddress("jteta",   jteta);
  tree->SetBranchAddress("jtphi",   jtphi);
  tree->SetBranchAddress("refdrjt", refdrjt);
  if (doflavor) tree->SetBranchAddress("refpdgid",refpdgid);

  //
  // book histograms of jtptvsptgen for all etas
  //
  TH2D * jtptvsptgen[84];
  for (int beta=0;beta<84;beta++){
    stringstream h2name;
    h2name << "jtptvsptgen_"<<beta;
    jtptvsptgen[beta+1] = new TH2D(h2name.str().c_str(),h2name.str().c_str(),NPtBins,vpt,NPtBins,vpt);
    jtptvsptgen[beta+1] = jtptvsptgen[beta+1];
  }

  RespVsPt_Bar = new TH2F("RespVsPt_Bar","RespVsPt_Bar",NPtBins,vpt,200,0,2);
  RespVsPt_Bar->Sumw2(); 
  RespVsPt_End = new TH2F("RespVsPt_End","RespVsPt_End",NPtBins,vpt,200,0,2);
  RespVsPt_End->Sumw2();
  RespVsPt_Fwd = new TH2F("RespVsPt_Fwd","RespVsPt_Fwd",NPtBins,vpt,200,0,2);
  RespVsPt_Fwd->Sumw2();
  //
  // fill histograms
  //
  unsigned int nevt = (unsigned int)tree->GetEntries();
  if (nEvents !=0) nevt = nEvents;
 
  cout<<algo<<"......"<<nevt<<" entries:"<<endl;
  for (unsigned int ievt=0;ievt< nevt;ievt++) {

    if (ievt % 100000 == 0) 
      cout<<ievt<<endl;
    tree->GetEntry(ievt);
	  
    // loop over jets
    for (unsigned char iref=0;iref<nrefmax;iref++){
	
      if(doflavor && refpdgid[iref]!=pdgid) continue;
      float eta    = jteta[iref];
      float pt     = jtpt[iref];
      float dr     = refdrjt[iref];
      if(drmax > 0 && dr > drmax) continue;
      float scale  = 1;
      if (scale*pt<30) continue;
      float ptgen  = refpt[iref];
      float relrsp = scale*jtpt[iref]/refpt[iref];
		  
      double weight = 1;
      if(weightHist!=0) weight = weightHist->GetBinContent(weightHist->FindBin(pt));

      //if (fabs(eta)<=1.3)
      if (eta>0 && eta<=0.087)
	{
	  RespVsPt_Bar->Fill(ptgen,relrsp,weight);
	}
      if ((fabs(eta)<=3.0) && (fabs(eta)>1.3))
	{
	  RespVsPt_End->Fill(ptgen,relrsp,weight);
	}
      if ((fabs(eta)<=5.0) && (fabs(eta)>3))
	{
	  RespVsPt_Fwd->Fill(ptgen,relrsp,weight); 
	}

    } 
  }// for events

}//whatever


////////////////////////////////////////////////////////////////////////////////
// implement local functions
////////////////////////////////////////////////////////////////////////////////

//______________________________________________________________________________
int getBin(double x, const double boundaries[NPtBins])
{
  int i;
  int n = NPtBins-1;
  if (n<=0) return -1;
  if (x<boundaries[0] || x>=boundaries[n])
    return -1;
  for(i=0;i<n;i++)
    {
      if (x>=boundaries[i] && x<boundaries[i+1])
        return i;
    }
  return 0; 
}

//______________________________________________________________________________
string getAlias(string s)
{
  if (s=="ic5calo")
    return "IC5Calo";
  else if (s=="ic5pf")
    return "IC5PF";
  else if (s=="ak5calo")
    return "AK5Calo";  
  else if (s=="ak5calol1")
    return "AK5Calol1";
  else if (s=="ak5calol1off")
    return "AK5Calol1off";
  else if (s=="ak7calo")
    return "AK7Calo";
  else if (s=="ak7calol1")
    return "AK7Calol1";
  else if (s=="ak5pf")
    return "AK5PF";
  else if (s=="ak5pfl1")
    return "AK5PFl1";
  else if (s=="ak5pfchs")
    return "AK5PF";   // change back to "AK5PFchs"
  else if (s=="ak5pfchsl1")
    return "AK5PFl1"; // change BACK TO AK5PFchsl1
  else if (s=="ak7pf")
    return "AK7PF";
  else if (s=="ak7pfl1")
    return "AK7PFl1";
  else if (s=="ak5jpt")
    return "AK5JPT";
  else if (s=="ak5jptl1")
    return "AK5JPTl1";
  else if (s=="ak7jpt")
    return "AK7JPT";
  else if (s=="ak7jptl1")
    return "AK7JPTl1";
  else if (s=="sc5calo")
    return "SC5Calo";
  else if (s=="sc5pf")
    return "SC5PF";
  else if (s=="sc7calo")
    return "SC5Calo";
  else if (s=="sc7pf")
    return "SC5PF";
  else if (s=="kt4calo")
    return "KT4Calo";
  else if (s=="kt4pf")
    return "KT4PF";
  else if (s=="kt6calo")
    return "KT6Calo";
  else if (s=="kt6pf")
    return "KT6PF";
  else
    return "unknown";
}

//---------------------------------------------------------------------
TF1 * getFitFunctionAlgo(string alg, double xmin, double xmax){

  TF1 * fabscor = 0;
  
  if (alg.find("pf")!=string::npos) {
    fabscor=new TF1("fit","[0]+[1]/(pow(log10(x),2)+[2])+[3]*exp(-[4]*(log10(x)-[5])*(log10(x)-[5]))",xmin,xmax);
    fabscor->SetParameter(0,0.5);
    fabscor->SetParameter(1,9.0);
    fabscor->SetParameter(2,8.0);
    fabscor->SetParameter(3,-0.3);
    fabscor->SetParameter(4,0.6);
    fabscor->SetParameter(5,1.0);
  }
  else if (alg.find("trk")!=string::npos) {
    fabscor=new TF1("fit","[0]+[1]*pow(x/500.0,[2])+[3]/log10(x)+[4]*log10(x)",xmin,xmax);
    fabscor->SetParameter(0,1.7);
    fabscor->SetParameter(1,0.7);
    fabscor->SetParameter(2,3.0);
    fabscor->SetParLimits(2,1,10);
    fabscor->SetParameter(3,0.0);
    fabscor->SetParameter(4,0.0);
  }
  else if (alg.find("jpt")!=string::npos) {
    fabscor=new TF1("fit","[0]+[1]*TMath::Erf([2]*(log10(x)-[3]))+[4]*exp([5]*log10(x))",xmin,xmax);
    fabscor->SetParameter(0,1.0);
    fabscor->SetParameter(1,1.0);
    fabscor->SetParameter(2,1.0);
    fabscor->SetParameter(3,1.0);
    fabscor->SetParameter(4,1.0);
    fabscor->SetParameter(5,1.0);
  }
  else if (alg.find("calo")!=string::npos) {
    fabscor=new TF1("fit","[0]+[1]/(pow(log10(x),[2])+[3])",xmin,xmax);
    fabscor->SetParameter(0,1.0);
    fabscor->SetParameter(1,5.0);
    fabscor->SetParameter(2,3.0);
    fabscor->SetParameter(3,3.0);
  }
  

  // multiply by the jtpt to get ptgen ??

  return fabscor;

}//getFitFunctionAlgo

// ---------------------------------------------------------------------------------
// This method write the L2L3 corrections to file for the current algo
void  printCorrectionsToFile(vector<TF1 *> ffit, string path, string era, string algo){

  string txtfilename = path+era+"_L2L3Minuit_"+getAlias(algo)+".txt";
  cout<<" Printing corrections to file "<<txtfilename<<endl;
  ofstream fout(txtfilename.c_str());
  fout.setf(ios::right);

  double ptmin = 10;
  double ptmax = 100000;
  // Loop over the vector
  for (unsigned int ieta = 0; ieta < ffit.size(); ieta++){

    if (!ffit[ieta]){
      cout<<"ERROR ieta="<<ieta<<" has a null pointer to TF1, skipping this eta"<<endl;
      continue;
    }

    // print the header for the first element of the vector
    if (ieta==0){
      string fnc_as_str = string(ffit[ieta]->GetExpFormula());
      fout<<"{1 JetEta 1 JetPt "<<fnc_as_str<<" Correction L2Relative}"<<endl;
    }//ieta=0
     
    // print the corrections for this eta
    double etamin, etamax;
    ffit[ieta]->GetRange(etamin,etamax);
    fout<<setw(11)<<etamin
	<<setw(11)<<etamax
	<<setw(11)<<(int)(ffit[ieta]->GetNpar()+2) //Number of parameters + 2 
	<<setw(12)<<ptmin
	<<setw(12)<<ptmax;
    for(int p=0; p<ffit[ieta]->GetNpar(); p++)
	fout<<setw(13)<<ffit[ieta]->GetParameter(p); // p0-p4

    // finish the line
    fout<<endl;    
    
  }// for eta bins

  // Close the file

  fout.close();
 
}// printCorrectionsToFile

