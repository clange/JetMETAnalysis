#!/bin/bash

# exit when any command fails, but allow variables not set (no -u); verbose
set -ex

# make cmsrel etc. work
shopt -s expand_aliases
export MY_BUILD_DIR=${PWD}
source /cvmfs/cms.cern.ch/cmsset_default.sh
cd /home/cmsusr
cmsrel CMSSW_5_3_32
mkdir -p CMSSW_5_3_32/src/JetMETAnalysis
mv ${MY_BUILD_DIR}/JetAnalyzers CMSSW_5_3_32/src/JetMETAnalysis
mv ${MY_BUILD_DIR}/JetUtilities CMSSW_5_3_32/src/JetMETAnalysis
cd CMSSW_5_3_32/src
cmsenv
scram b
